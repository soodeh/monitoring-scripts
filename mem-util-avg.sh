#/bin/bash

# This script calculates the average of the memory utilization in a period of given interval value as an input, it invokes "mem-usage.sh" script every second. 
# It generates a csv file which include the mem average values and the corresponding timestamps.
# This script is written as one of the inputs of the resource controller.
# Authors: Soodeh Farokhi & Ewnetu Lakew
# Date: April 2015
# Status: Finalized!

interval=$1
output_file=mem-avg.csv

if [ -z "$1" ]
then
echo "Missing input value (control interval): e.g., ./mem-util-avg.sh 5"
exit 1
fi

echo timestamp, avg-mem-util > $output_file
mem_util=`./mem-usage.sh`
echo $mem_util
#the first line is the instant value of the mem utilization
echo $(date +%s), $mem_util >> $output_file
while true
do
	mem_util_sum=0
	for i in `seq 1 $interval`;
	do
		#echo loop number $i 
		mem_util=`./mem-usage.sh`
		#echo $(date +%s), $mem_util >> all.csv
		#mem_util_sum=$(($mem_util_sum + $mem_util))
		mem_util_sum=`bc <<< "scale=4; $mem_util_sum + $mem_util"`
		#echo memory util = $mem_util
		#echo memory util sum = $mem_util_sum
		sleep 0.5
	done 
	#mem_util_avg=$(($mem_util_sum/$interval))
	mem_util_avg=`bc <<< "scale=4; $mem_util_sum/$interval"`
	echo  $mem_util_avg
	echo $(date +%s), $mem_util_avg >> $output_file
done

