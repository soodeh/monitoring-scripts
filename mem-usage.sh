#!/bin/bash

# This script measure the memory utilization every seconds and prints out the corresponding integer value 
# It is invoked by "mem_util_avg.sh" script
# This script is written as one of the inputs of the resource controller.
# Authors: Soodeh Farokhi & Ewnetu Lakew
# Date: April 2015
# Status: Finalized!

mem_total=`cat /proc/meminfo | grep MemTotal |awk '{print($2)}'`
mem_free=`cat /proc/meminfo | grep MemFree |awk '{print($2)}'`
mem_buffers=`cat /proc/meminfo | grep Buffers |awk '{print($2)}'`
mem_cached=`cat /proc/meminfo | grep  Cached  | head -n 1|awk '{print($2)}'`
mem_usage=`bc <<< "scale=4; (($mem_total - ($mem_free + $mem_buffers + $mem_cached)) / $mem_total) * 100"`

echo $mem_usage
