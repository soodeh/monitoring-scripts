#!/bin/bash     

# This script measure the average cpu utilization in a given interval (as the input) and prints out the corresponding integer value 
# This script is written as one of the inputs of the resource controller.
# Authors: Soodeh Farokhi & Ewnetu Lakew
# Date: April 2015
# Status: Finalized!


#The CPU Load can be calculated with information from "/proc/stat" using the following formula:
#CPU Load  = (system + user + nice + iowait + irq + softirq)/ (user + nice + system + idle + iowait + irq + softirq)

interval=$1
output_file=cpu-avg.csv

if [ -z "$1" ]
then
echo "Missing input value (control interval): e.g., ./cpu-util-avg.sh 5"
exit 1
fi


echo timestamp, avg-cpu-util > $output_file


PREV_TOTAL=0
PREV_IDLE=0

while true; do
  # Get the total CPU statistics, discarding the 'cpu ' prefix.
  CPU=(`sed -n 's/^cpu\s//p' /proc/stat`)
  IDLE=${CPU[3]} # Just the idle CPU time.

  # Calculate the total CPU time.
  TOTAL=0
  for VALUE in "${CPU[@]}"; do
    let "TOTAL=$TOTAL+$VALUE"
  done

  # Calculate the CPU usage since we last checked.
  let "DIFF_IDLE=$IDLE-$PREV_IDLE"
  let "DIFF_TOTAL=$TOTAL-$PREV_TOTAL"
  let "DIFF_USAGE=(1000*($DIFF_TOTAL-$DIFF_IDLE)/$DIFF_TOTAL+5)/10"
  echo $DIFF_USAGE
#  echo -en "\rCPU: $DIFF_USAGE%  \b\b"

  # Remember the total and idle CPU times for the next check.
  PREV_TOTAL="$TOTAL"
  PREV_IDLE="$IDLE"

  # Wait before checking again.
  echo $(date +%s), $DIFF_USAGE >> $output_file
  sleep $interval
done

echo $cpu_usage




